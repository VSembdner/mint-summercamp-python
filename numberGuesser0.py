import random

def zahlenRaten(maxNumber):
    numToGuess = random.randint(0, maxNumber)
    numOfGuess = 0
    while True:
        try:
            curGuess = int(input("Bitte Zahl eingeben: "))
            numOfGuess += 1
        except:
            print(f"Falsche Eingabe")
            return
        if curGuess == numToGuess:
            print(f"RICHTIG, die Zahl war {numToGuess} und wurde mit {numOfGuess} Rateversuchen erraten!")
            return
        elif curGuess > numToGuess:
            print(f"FALSCH, die Zahl ist kleiner als {curGuess}")
        elif curGuess < numToGuess:
            print(f"FALSCH, die Zahl ist größer als {curGuess}")

try:
    zahlenRaten(int(input("Wie groß soll die zuratene Zahl sein: ")))
except:
    print("Falsche Eingabe!")