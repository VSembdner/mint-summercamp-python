import pygame as pg
import colorsbase

PURPLE = (160, 32, 240)
ORANGERED = (255, 69, 0)

WINDOW_WIDTH = 1000
WINDOW_HEIGHT = 1000
pg.init()

screen = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pg.display.set_caption("Title of Game")
clock = pg.time.Clock()

#game main loop

running = True
while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
            print("Game will be closed!")
    screen.fill(ORANGERED)
    pg.display.flip()
    clock.tick(60)

pg.quit()