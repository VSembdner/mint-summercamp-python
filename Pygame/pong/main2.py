import pygame
from colorcodes import *
from pygame.locals import *
import sys
import random

SCREEN_WIDTH = 40       # in squares
SCREEN_HEIGHT = 30
SQUARE_SIZE = 20        # in pixel

def pxl(number_of_squares):
    """        
    number of squares -> number of pixels    
    """    
    return SQUARE_SIZE * number_of_squares

def fill_cell(x, y, color=BLUE):
    pygame.draw.rect(screen, color, [pxl(x), pxl(y), pxl(1), pxl(1)])

# initialization

pygame.init()
screen = pygame.display.set_mode( (pxl(SCREEN_WIDTH), pxl(SCREEN_HEIGHT)) )
pygame.display.set_caption("Titel des Spiels")
clock = pygame.time.Clock()
ticks = 0
# game main loop
running = True
while running:
    # read user inputs #################################################################    
    for event in pygame.event.get():
        if event.type == QUIT or \
            event.type == KEYDOWN and event.key == K_ESCAPE:
            running = False           
            print("Spiel wird beendet!")
    # game logic #######################################################################    # draw frame #######################################################################    
    screen.fill( ORANGERED )
    # test 1    
    # fill_cell(0, 0)    
    # fill_cell(SCREEN_WIDTH-1, SCREEN_HEIGHT-1, LIMEGREEN)    
    # test 2    
    # [ (12. 23, RED), (5, 22, ORANGE), (2,34,BLUE)   ]    
    if ticks % 60 == 0:
        squares = [ ( random.randint(0, SCREEN_WIDTH-1), random.randint(0, SCREEN_HEIGHT-1), random.choice(ALL_COLOR_CODES) ) for i in range(100) ]
    for x, y, color in squares:
        fill_cell(x, y, color)

    if ticks % 60 == 0:
        circles = [ ( random.randint(0, SCREEN_WIDTH-1), random.randint(0, SCREEN_HEIGHT-1), random.choice(ALL_COLOR_CODES) ) for i in range(100) ]
    for x, y, color in circles:
        fill_cell(x, y, color)
    ####################################################################################    
    pygame.display.flip()
    clock.tick(60)
    ticks += 1

pygame.quit()
sys.exit()