import pygame as pg
from colorcodes import *

WINDOW_WIDTH = 1000
WINDOW_HEIGHT = 1000
pg.init()

# initialization
    
screen = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pg.display.set_caption("Pong")
clock = pg.time.Clock()

BALL_DIAMETER = 20
ball_x = (WINDOW_WIDTH - BALL_DIAMETER) // 2
ball_y = (WINDOW_HEIGHT- BALL_DIAMETER) // 2
delta_ball_x = 5
delta_ball_y = 3

PLAYER_WIDTH = 20
PLAYER_HEIGHT = 100
player_x = 50
player_y = 100

ball = pg.draw.ellipse(screen, DARKBLUE, [ball_x, ball_y, BALL_DIAMETER, BALL_DIAMETER])
player = pg.draw.rect(screen, PURPLE, [player_x, player_y, PLAYER_WIDTH, PLAYER_HEIGHT])
delta_player_y = 0

#game main loop

running = True
while running:

    # read user inputs #################################################################

    for event in pg.event.get():
        if event.type == pg.QUIT or event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
            running = False
            print("Game will be closed!")
        elif event.type == pg.KEYDOWN:
            if  event.key == pg.K_UP:
                print("Arrow up")
                delta_player_y = -3
            elif event.key == pg.K_DOWN:
                print("Arrow down")
                delta_player_y = 3

    # game logic #######################################################################

    ball_x += delta_ball_x
    ball_y += delta_ball_y


    #check border collision
    if ball_x < 0:
        delta_ball_x *= -1
        ball_x = 0

    if ball_x > WINDOW_WIDTH - BALL_DIAMETER:
        delta_ball_x *= -1
        ball_x = WINDOW_WIDTH - BALL_DIAMETER

    if ball_y < 0:
        delta_ball_y *= -1
        ball_y = 0

    if ball_y > WINDOW_HEIGHT - BALL_DIAMETER:
        delta_ball_y *= -1
        ball_y = WINDOW_HEIGHT - BALL_DIAMETER

    # ball-player collision detection
    if player.colliderect(ball):
        delta_ball_x *= -1
        ball_x = player_x + PLAYER_WIDTH

    #player-border collision
    if player_y < 0:
        delta_player_y = 0
        player_y = 0
    if player_y > WINDOW_HEIGHT - PLAYER_HEIGHT:
        delta_player_y = 0
        player_y = WINDOW_HEIGHT - PLAYER_HEIGHT

    player_y += delta_player_y
    

    # draw frame #######################################################################

    screen.fill(ORANGERED)
    ball = pg.draw.ellipse(screen, DARKBLUE, [ball_x, ball_y, BALL_DIAMETER, BALL_DIAMETER])
    player = pg.draw.rect(screen, PURPLE, [player_x, player_y, PLAYER_WIDTH, PLAYER_HEIGHT]) 

    ####################################################################################

    pg.display.flip()
    clock.tick(60)

pg.quit()