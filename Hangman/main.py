import random
from hangmanFigures import statusList

class Game(object): #class for Hangman-game
    def __init__(self, wordList): #constructor
        self.wordToGuess = wordList[random.randint(0, len(wordList)-1)].upper() #word is picked random out of word array
        self.wordToGuess = list(self.wordToGuess)
        self.solution = ["_"] * len(self.wordToGuess)
        self.wrongGuess = 0 
        self.maxWrongGuess = 10
    
    def Guess(self, curGuess): #Guess one char
        try:
            curGuess = curGuess.upper()[0]
            if not curGuess in self.wordToGuess:
                self.wrongGuess += 1
            else:
                for i in range(len(self.wordToGuess)):
                    if self.wordToGuess[i] == curGuess:
                        self.solution[i] = curGuess
            print(statusList[self.wrongGuess])
        except:
            print("Falsche Eingabe!")
    
    def solve(self): #solve the word
        while self.wrongGuess < self.maxWrongGuess and not self.solution == self.wordToGuess: #as long as wrong Guess limit isn't reached and the solution not equal the word
            print(self.solution)
            self.Guess(input("Welchen Buchstaben möchtest du als nächstes raten: "))
            if self.solution == self.wordToGuess:
                print("GEWONNEN!")
            elif self.wrongGuess >= self.maxWrongGuess:
                print(f"VERLOREN!, das Wort war {self.wordToGuess}")

def play(): #play multiple games
    while True:
        nextGameInput = input("Möchtest du spielen?(j/n): ")
        if nextGameInput == "j":
            difficulty = input("Einfach oder schwer?(easy/hard): ")
            if difficulty == "easy":
                with open('./words/germanCitiesEasy.txt') as f: #open text file of german cities
                    wordList = f.read().splitlines() #reads german cities as Array
            elif difficulty == "hard":
                with open('./words/wordsHard.txt') as f: #open text file of german words
                    wordList = f.read().splitlines() #reads german words as Array
            game = Game(wordList) #new game with selected wordList
            game.solve() #solve(play) the game
        elif nextGameInput == "n":
            return #ends playing

if __name__ == "__main__": #if script is executed
    play() #start playing